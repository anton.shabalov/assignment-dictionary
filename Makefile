flags = felf64
linker = ld
compiler = nasm


all: main.o lib.o dict.o
	$(linker) main.o lib.o dict.o -o lab2

main.o: colon.inc main.asm
	$(compiler) -$(flags) -o main.o main.asm

lib.o: lib.asm
	$(compiler) -$(flags) -o lib.o lib.asm

dict.o: dict.asm
	$(compiler) -$(flags) -o dict.o dict.asm

clean:
	rm -rf *.o lab2
