%define key 0 

%macro colon 2
	%ifid %2
		%2:
			dq key
			%define key %2
	%else
		%fatal 'problem with %2'
	%endif
	%ifstr %1
		db %1, 0
	%else
		%fatal 'problem with %1'
	%endif
%endmacro
