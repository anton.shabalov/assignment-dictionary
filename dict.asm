section .text
%define rbytes 8
extern string_equals
global find_word
find_word:
	.start:
		push rsi
		push rdi
		add rsi,rbytes
		call string_equals
		pop rdi
		pop rsi
		test rax,1
		jz .good
		mov rsi,[rsi]
		test rsi,0
		jz .bad
		jmp .start
		
	.good:
		mov rax,rsi
		jmp .end
	.bad:
		mov rax,0
	.end:
		ret
		
	
		
