section .text
global exit
global print_string
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
   xor rax, rax
   mov     rax, 60
   syscall
   ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .nextSimbol:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .nextSimbol
    .end:
        ret
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret




; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push 0
    mov rax, rdi
    mov rdi, rsp
    mov rdx,0
    mov dl,0
    sub rsp, 50
    mov r8, 10
    dec rdi
.step:
    div r8
    add rdx, '0'
    dec rdi
    mov byte[rdi], dl
    mov rdx, 0
    cmp rax, 0
    jne .step
    call print_string
    add rsp, 58
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    mov rax,rdi
    cmp rax,0
    js .negativ
        call print_uint
        jmp .end
    .negativ:
        push rdi
        mov rdi, '-'
        call print_char
        pop rax
        neg rax
        mov rdi,rax
        call print_uint
    .end:
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
push rdi
    push rsi
    call string_length
    mov r10, rax
    mov rdi, rsi
    call string_length
    mov r9, rax
    pop rsi
    pop rdi
    cmp r10, r9
    jne .noGood
	xor rcx, rcx
	xor r9, r9
	xor r10, r10
.check:
	mov r9b, byte[rdi+rcx]
	mov r10b, byte[rsi+rcx]
        inc rcx
	cmp r9b, r10b
	jne .noGood
        cmp r9b, 0
        je .good
        jmp .check
.noGood:
	mov rax, 0
	jmp .exit
.good:
        mov rax, 1
.exit:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax,0
    mov rdi,0
    mov rdx,1
    push 0
    mov rsi,rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
  xor rdx, rdx

.check:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0    
    je .good    
    cmp rdx, 0
    jne .read_all
    cmp rax, 0x9
    je .check
    cmp rax, 0xa
    je .check
    cmp rax, 0x20
    je .check
.read_all:
    cmp rdx, rsi
    je .err
    cmp rax, 0x9
    je .good
    cmp rax, 0xa
    je .good
    cmp rax, 0x20
    je .good 
    mov [rdi+rdx], al    
    inc rdx
    jmp .check
.good:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret
.err:
    xor rax,rax
    ret



 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rdx, rdx
    mov r8,10
    mov r9,0
.step:
    mov sil, [rdi+r9]
    cmp sil, '0'
    jl .end
    cmp sil, '9'
    jg .end
    inc r9
    sub sil, '0'
    mul r8
    add rax, rsi
    jmp .step
.end:
    mov rdx, r9
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov r8,rdi
    cmp  byte [r8], '-'
    je .nePolojitelnoe
    jmp parse_uint
    ret
.nePolojitelnoe:
    inc r8
    mov rdi,r8
    call parse_uint
    test rdx,rdx
    je .ex
    neg rax
    inc rdx
.ex
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
xor r9,r9
xor rax, rax
    .nextSimbol:
        cmp byte [rdi+rax], 0
        je .startCopy
        inc rax
        jmp .nextSimbol
.startCopy:
 cmp rax, rdx
 jg .error
 mov rcx, 0
 .step:
  mov r9b, byte [rdi+rcx]
  mov byte [rsi+rcx], r9b
  cmp rcx, rax
  je .end
  inc rcx
  jmp .step
 .error:
  mov rax,0
 .end:
  ret

