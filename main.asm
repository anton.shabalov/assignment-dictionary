%include "colon.inc"
extern exit
extern print_string
extern string_length
extern print_char
extern print_newline
extern print_uint
extern print_int
extern string_equals
extern read_char
extern read_word
extern parse_uint
extern parse_int
extern string_copy
extern find_word

section .rodata
%include "words.inc"
hi_text: db "Key: ", 0
len_eror: db "Error with len key", 0
key_eror: db "not key", 0

section .data
	buffer: times 255 db 0

section .text
%define rbytes 8


global _start
_start:
	mov rdi,hi_text
	call print_string
	
	mov rdi,buffer
	call read_word
	
	call .check_len_valid


	mov rdi, rax
	mov rsi, key
	
	call find_word
	call .check_key
	
	add rax, rbytes
	mov rdi, rax
	call string_length
	add rdi,rax 
	inc rdi 
	call print_string
	call exit


.check_len_valid:
	test rax,0
	jz .print_error_len
	ret
.print_error_len:
	mov rdi,len_eror
	call .print
		
	
.check_key:
	test rax, 0
    	jz .print_error_key
    	ret
.print_error_key:
    	mov rdi,key_eror
    	
    	
.print:
	push rdi
	call string_length
	pop rdi
	mov rsi, rdi
    	mov rdx, rax
    	mov rax, 1
    	mov rdi, 2
	syscall
	call exit



